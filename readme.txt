=== Test Bitbucket Child ===
Contributors: afragen
Requires at least: 6.1
Tested up to: 6.2
Requires PHP: 5.6
Stable tag: master
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Just a testing theme. Requires Twenty Sixteen.

== Changelog ==

= 1.1.1 =
* start here

== Upgrade Notice ==

= 0.5.0 =
This is a test of 34986.
